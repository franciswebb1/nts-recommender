# README #

## Live site - http://nts-angular.s3-website.us-east-2.amazonaws.com/ ##

This application aims to provide show recommendations to users of the NTS site (https://www.nts.live/about).

### Get started - Flask Server ###

* Currently using Python 3.6
* Install the requirements in `requirements.txt`
* Run the `recommender.py` within the python directory to start up the Flask server
* Test it out by going to `localhost:5002/` in your browser

### Get started - Angular  ###

* Run `npm install` in the angular directory
* Run `ng serve` to start the application
* Test it out by going to `localhost:4200`

