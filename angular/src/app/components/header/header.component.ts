import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  @Output() displaySuggestions = new EventEmitter();
  @Output() reset = new EventEmitter();
  
  constructor() { }

  ngOnInit() {
  }

  getSuggestions() {
    this.displaySuggestions.emit(true);
  }

  uploadComplete() {
    this.displaySuggestions.emit('complete');
  }

  resetApp() {
    this.reset.emit(true);
  }

  resetComplete() {
    this.reset.emit('complete');
  }

}
