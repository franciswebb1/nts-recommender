import { Component, OnInit, Output } from '@angular/core';
import { EventEmitter } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-user-input',
  templateUrl: './user-input.component.html',
  styleUrls: ['./user-input.component.css']
})
export class UserInputComponent implements OnInit {
  @Output() uploaded = new EventEmitter();

  url: string;
  host: string;
  show: string;
  goodResponse: boolean;
  httpOptions: object = {
    headers: 'string',
    responseType: 'json'
  }
  constructor(private httpClient: HttpClient) { }

  ngOnInit(): void {
  }

  public getRecommendations(url) {
    var subString = url.split( '/' );
    this.host = subString[4];
    this.show = subString[6];
    this.httpClient.get('http://flask-env.eba-7iietsfp.us-east-2.elasticbeanstalk.com/api/v1/recommender/'+this.host+'/'+this.show)
    .subscribe((data) => {
      console.log(data);
      this.uploaded.emit(data);
    });
  }

  uploadComplete() {
    this.uploaded.emit('complete');
  }

}
