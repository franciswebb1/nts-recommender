import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-suggestion',
  templateUrl: './suggestion.component.html',
  styleUrls: ['./suggestion.component.css']
})
export class SuggestionComponent implements OnInit {
  suggestedShows = [
    ]

  shows = [
    {show: 'tiffany-calver-19th-april-2017', url: 'https://www.nts.live/shows/tiffany-calver/episodes/tiffany-calver-19th-april-2017', img: 'https://media2.ntslive.co.uk/resize/1600x1600/5814d7e2-a5b1-4c08-880d-b965f32291c7_1478476800.jpeg'},
    {show: 'the-do-you-breakfast-show-5th-february-2020', url: 'https://www.nts.live/shows/the-do-you-breakfast-show/episodes/the-do-you-breakfast-show-5th-february-2020', img: 'https://media2.ntslive.co.uk/resize/1600x1600/1586543f-990a-4f72-8103-e67bf9bb65a6_1560816000.jpeg'},
    {show: 'four-tet-and-floating-points-18th-october2016', url: 'https://www.nts.live/shows/four-tet/episodes/four-tet-and-floating-points-18th-october2016', img: 'https://media2.ntslive.co.uk/resize/1600x1600/ed074fd9-6c62-4906-9c6a-f20756639847_1502841600.jpeg'},
    {show: 'martelo-7th-august-2019', url: 'https://www.nts.live/shows/martelo/episodes/martelo-7th-august-2019', img: 'https://media2.ntslive.co.uk/resize/1600x1600/9900afcb-e874-444a-8c94-724072432f8d_1565740800.jpeg'},
    {show: 'donna-leake-25th-june-2019', url: 'https://www.nts.live/shows/donna-leake/episodes/donna-leake-25th-june-2019', img: 'https://media2.ntslive.co.uk/resize/1600x1600/ce6b9bc0-fd05-4d41-a0a4-7fe1e41cff8b_1561507200.jpeg'},
    {show: 'brav-18th-november-2019', url: 'https://www.nts.live/shows/brav/episodes/brav-18th-november-2019', img: 'https://media2.ntslive.co.uk/resize/1600x1600/5ce07b09-a5a9-459f-bc8e-243ae20e2dd3_1491436800.jpeg'},
    {show: 'world-in-flo-motion-23rd-march-2020', url: 'https://www.nts.live/shows/world-in-flo-motion/episodes/world-in-flo-motion-23rd-march-2020', img: 'https://media2.ntslive.co.uk/resize/1600x1600/b2cb738b-445e-4f7a-a7b0-69b6728ce154_1585612800.jpeg'},
    {show: 'born-n-bread-15th-february-2020', url: 'https://www.nts.live/shows/bornnbread/episodes/born-n-bread-15th-february-2020', img: 'https://media2.ntslive.co.uk/resize/1600x1600/a781097a-d250-4a82-9737-94bb229aa948_1450956650.jpg'},
    {show: 'meme-gold-23rd-march-2019', url: 'https://www.nts.live/shows/meme-gold/episodes/meme-gold-23rd-march-2019', img: 'https://media2.ntslive.co.uk/resize/1600x1600/239b10e9-ea26-4167-8b93-8046ad43f069_1553299200.jpeg'},
    // Above this line are just a handful of shows I've enjoyed, below were calculated by splitting the dataset into 
    // 16 clusters and taken two random samples from each to get a wide ditrsibution
    // In future, these could be fetched dynamically...
    {url: 'https://www.nts.live/shows/body-motion/episodes/body-motion-19th-july-2016/', show:  'body-motion-19th-july-2016', img: 'https://media2.ntslive.co.uk/resize/1600x1600/66a19da1-e923-427c-8afe-a6d99bb17b07_1468972800.jpeg'},
    {url: 'https://www.nts.live/shows/skyapnea/episodes/s-k-y-a-p-n-e-a-22nd-april-2019/', show:  's-k-y-a-p-n-e-a-22nd-april-2019', img: 'https://media2.ntslive.co.uk/resize/1600x1600/868fdddd-dd51-4304-bc7f-cb287a63d9f7_1556064000.jpeg'},
    {url: 'https://www.nts.live/shows/alien-jams/episodes/alien-jams-w-chloe-frieda-29th-october-2019/', show:  'alien-jams-w-chloe-frieda-29th-october-2019', img: 'https://media2.ntslive.co.uk/resize/1600x1600/9592177f-04ae-49fe-8ebb-33644392c1aa_1559001600.jpeg'},
    {url: 'https://www.nts.live/shows/lily-mercer/episodes/lily-mercer-14th-june-2019/', show:  'lily-mercer-14th-june-2019', img: 'https://media2.ntslive.co.uk/resize/1600x1600/c3bf7d2c-61d0-4774-ab28-29149e060fc3_1560729600.jpeg'},
    {url: 'https://www.nts.live/shows/country-hayride/episodes/country-hayride-10th-february-2019/', show:  'country-hayride-10th-february-2019', img: 'https://media2.ntslive.co.uk/resize/1600x1600/8169a8d8-78c6-4d5e-a471-31a491bac5b9_1549929600.jpeg'},
    {url: 'https://www.nts.live/shows/jon-k-presents-peking-spring/episodes/peking-lobsters-w-jon-k-tom-boogizm-19th-march-2017/', show:  'peking-lobsters-w-jon-k-tom-boogizm-19th-march-2017', img: 'https://media2.ntslive.co.uk/resize/1600x1600/d18236da-c3b5-4a26-b379-7a0b4f0b8def_1490054400.jpeg'},
    {url: 'https://www.nts.live/shows/subterranean-odyssey/episodes/subterranean-odyssey-22nd-november-2017/', show:  'subterranean-odyssey-22nd-november-2017', img: 'https://media2.ntslive.co.uk/resize/1600x1600/d6f1bb6b-3231-4779-97f8-d939ea7b3b9f_1476835200.jpeg'}, 
    {url: 'https://www.nts.live/shows/house-of-trax/episodes/house-of-trax-w-farhan-yassin-guest-mix-25th-september-2015/', show:  'house-of-trax-w-farhan-yassin-guest-mix-25th-september-2015', img: 'https://media2.ntslive.co.uk/resize/1600x1600/0d5849ee-b591-4e5d-89aa-6586d6a40b48_1432152244.jpg'}, 
    {url: 'https://www.nts.live/shows/fullhouse/episodes/full-house-7th-june-2015/', show:  'full-house-7th-june-2015', img: 'https://media2.ntslive.co.uk/resize/1600x1600/ba3bfc1e-ebb7-4f38-a803-086c2892ff19_1457395200.jpeg'}, 
    {url: 'https://www.nts.live/shows/the-witching-hour/episodes/the-witching-hour-3rd-march-2016/', show:  'the-witching-hour-3rd-march-2016', img: 'https://media2.ntslive.co.uk/resize/1600x1600/31b86bec-c452-442d-8422-315ff61c8211_1459810800.jpeg'},
    {url: 'https://www.nts.live/shows/andy-votel/episodes/andy-votel-11th-november-2018/', show:  'andy-votel-11th-november-2018', img: 'https://media2.ntslive.co.uk/resize/1600x1600/277cd9ab-e144-4aaf-844a-8a3f557a952b_1465513200.jpeg'}, 
    {url: 'https://www.nts.live/shows/soundsofthedawn/episodes/sounds-of-the-dawn-4th-january-2020/', show:  'sounds-of-the-dawn-4th-january-2020', img: 'https://media2.ntslive.co.uk/resize/1600x1600/e85ce52f-8bb5-410a-a0cc-6a4fae8d5958_1578009600.png'}, 
    {url: 'https://www.nts.live/shows/francesco-fusaro/episodes/tafelmuzik-6th-april-2020/', show:  'tafelmuzik-6th-april-2020', img: 'https://media2.ntslive.co.uk/resize/1600x1600/862522b9-a20a-4e9a-99d5-d4bca786cd2c_1521072000.jpeg'},
    {url: 'https://www.nts.live/shows/ttb/episodes/ttb-20th-november-2018/', show:  'ttb-20th-november-2018', img: 'https://media2.ntslive.co.uk/resize/1600x1600/40182611-7f0c-4327-84bb-724604a4b30a_1542758400.png'}, 
    {url: 'https://www.nts.live/shows/slow-hand-clutches/episodes/slow-hand-clutches-11th-april-2020/', show:  'slow-hand-clutches-11th-april-2020', img: 'https://media2.ntslive.co.uk/resize/1600x1600/0baee134-b058-4268-ba82-a077e380827b_1586390400.jpeg'},
    {url: 'https://www.nts.live/shows/56-djs/episodes/whodis-w-ssakanoi-ta-raach-9th-may-2018/', show:  'whodis-w-ssakanoi-ta-raach-9th-may-2018', img: 'https://media2.ntslive.co.uk/resize/1600x1600/ab6f1479-7a75-43af-9f43-f9d24000c304_1526342400.png'},
    {url: 'https://www.nts.live/shows/56-djs/episodes/whodis-29th-january-2020/', show:  'whodis-29th-january-2020', img: 'https://media2.ntslive.co.uk/resize/1600x1600/f1937f4a-d7d8-43d2-88c3-b07e45a7874c_1499212800.png'},
    {url: 'https://www.nts.live/shows/dont-trip/episodes/dont-trip-w-special-guest-lisa-lavery-20th-october-2014/', show:  'dont-trip-w-special-guest-lisa-lavery-20th-october-2014', img: 'https://media2.ntslive.co.uk/resize/1600x1600/aec3c587-61ff-43a3-a13e-e563e03d5822_1436365316.jpg'},
    {url: 'https://www.nts.live/shows/literaryfriction/episodes/literaryfriction18022014/', show:  'literaryfriction18022014', img: 'https://media2.ntslive.co.uk/resize/1600x1600/593690b8-0015-4b52-90b7-78277da11269_1560211200.jpeg'},
    {url: 'https://www.nts.live/shows/anthonychalmers/episodes/anthony-chalmers-4th-april-2016/', show:  'anthony-chalmers-4th-april-2016', img: 'https://media2.ntslive.co.uk/resize/1600x1600/bd9541be-b83b-46ac-b720-35234b16d1fe_1457395200.jpeg'},
    {url: 'https://www.nts.live/shows/diddywah/episodes/diddy-wah-18th-march-2019/', show:  'diddy-wah-18th-march-2019', img: 'https://media2.ntslive.co.uk/resize/1600x1600/329b34b5-80dd-4977-8579-c0657e74b7d3_1458604800.jpeg'},
    {url: 'https://www.nts.live/shows/image-search/episodes/image-search-9th-october-2019/', show:  'image-search-9th-october-2019', img: 'https://media2.ntslive.co.uk/resize/1600x1600/6e21c8f6-d21b-4545-882e-338474d87e95_1556150400.jpeg'},
    {url: 'https://www.nts.live/shows/terrible-records/episodes/terrible-records-7th-febuary-2018/', show:  'terrible-records-7th-febuary-2018', img: 'https://media2.ntslive.co.uk/resize/1600x1600/610a0587-707f-4baa-a9db-a39cfa1f66b3_1585872000.jpeg'},
    {url: 'https://www.nts.live/shows/ashtrejinkins/episodes/buried-light-w-ashtrejenkins-20th-august-2019/', show:  'buried-light-w-ashtrejenkins-20th-august-2019', img: 'https://media2.ntslive.co.uk/resize/1600x1600/cd504d74-78f7-4901-b164-e6d332c4c816_1566345600.jpeg'},
    {url: 'https://www.nts.live/shows/chloedees/episodes/chloedees-5th-jan-2020/', show:  'chloedees-5th-jan-2020', img: 'https://media2.ntslive.co.uk/resize/1600x1600/8626292c-e880-40ce-883a-db6157a0b70a_1578268800.jpeg'},
    {url: 'https://www.nts.live/shows/the-windmills-of-your-mind/episodes/the-windmills-of-your-mind-w-taylor-rowley-24th-november-2019/', show:  'the-windmills-of-your-mind-w-taylor-rowley-24th-november-2019', img: 'https://media2.ntslive.co.uk/resize/1600x1600/63dee53e-8b21-4471-b309-40edf0372564_1549843200.jpeg'},
    {url: 'https://www.nts.live/shows/michelle/episodes/michelle-6th-october-2018/', show:  'michelle-6th-october-2018', img: 'https://media2.ntslive.co.uk/resize/1600x1600/498c41e7-b228-43ba-95c9-182de0721aef_1538784000.jpeg'},
    {url: 'https://www.nts.live/shows/lung-dart/episodes/lung-dart-5th-march-18/', show:  'lung-dart-5th-march-18', img: 'https://media2.ntslive.co.uk/resize/1600x1600/7fb1a01b-be6e-4dcc-9ca8-33841524bb4c_1563148800.jpeg'},
    {url: 'https://www.nts.live/shows/bo-ningen/episodes/bo-ningen-10th-july-2018/', show:  'bo-ningen-10th-july-2018', img: 'https://media2.ntslive.co.uk/resize/1600x1600/b7d43c96-55f4-4ce5-b22b-7cd7b3691cb8_1531180800.jpeg'},
    {url: 'https://www.nts.live/shows/reverie/episodes/reverie-26th-september-2017/', show:  'reverie-26th-september-2017', img: 'https://media2.ntslive.co.uk/resize/1600x1600/0155640b-2c73-48e8-9986-1781a652e84f_1506988800.jpeg'},
    {url: 'https://www.nts.live/shows/daveid/episodes/dave-id-30th-july-2018/', show:  'dave-id-30th-july-2018', img: 'https://media2.ntslive.co.uk/resize/1600x1600/a8ac953f-5e8f-49c7-bf20-6afb330ed23c_1460329200.jpeg'},
    {url: 'https://www.nts.live/shows/yayayi/episodes/athenian-marketplace-29th-august-2019/', show:  'athenian-marketplace-29th-august-2019', img: 'https://media2.ntslive.co.uk/resize/1600x1600/9e68b311-7d58-4516-a7b5-7b9b9954c394_1480291200.jpeg'},
    {url: 'https://www.nts.live/shows/dj-harrison/episodes/dj-harrison-2nd-november-2017/', show:  'dj-harrison-2nd-november-2017', img: 'https://media2.ntslive.co.uk/resize/1600x1600/2a31f709-b7bb-472d-a8ab-203620cd3d91_1490572800.jpeg'},
    {url: 'https://www.nts.live/shows/segabodega/episodes/sega-bodega-soundtrack-series-7th-june-2018/', show:  'sega-bodega-soundtrack-series-7th-june-2018', img: 'https://media2.ntslive.co.uk/resize/1600x1600/d7e81a9a-208b-4613-a226-779418f4a7ac_1475539200.jpeg'},
    {url: 'https://www.nts.live/shows/questing-w-zakia/episodes/questing-w-zakia-16th-february-2019/', show:  'questing-w-zakia-16th-february-2019', img: 'https://media2.ntslive.co.uk/resize/1600x1600/8e9ca89e-7524-40f6-9f33-2ef0516e57e8_1550448000.png'},
    {url: 'https://www.nts.live/shows/connah/episodes/still-life-w-connah-12th-may-2018/', show:  'still-life-w-connah-12th-may-2018', img: 'https://media2.ntslive.co.uk/resize/1600x1600/24487f37-2954-4c96-ad52-b8c751364a96_1526083200.jpeg'},
    {url: 'https://www.nts.live/shows/andy-butler/episodes/andy-butler-14th-september-2018/', show:  'andy-butler-14th-september-2018', img: 'https://media2.ntslive.co.uk/resize/1600x1600/bc2ec8d7-6800-443f-b00d-e03f28aa18bf_1516665600.jpeg'},
    {url: 'https://www.nts.live/shows/jaro-sounder/episodes/jaro-sounder-17th-march-2019/', show:  'jaro-sounder-17th-march-2019', img: 'https://media2.ntslive.co.uk/resize/1600x1600/be8de426-8411-4137-ac1d-322453bb155c_1486512000.jpeg'},
    {url: 'https://www.nts.live/shows/total-stasis/episodes/total-stasis-9th-october-2017/', show:  'total-stasis-9th-october-2017', img: 'https://media2.ntslive.co.uk/resize/1600x1600/e7690048-5441-4c8b-a712-b3a4c8162eee_1507507200.jpeg'},
    {url: 'https://www.nts.live/shows/meandyou/episodes/meandyou-with-andrew-lyster-huerco-s-31-july-2013/', show:  'meandyou-with-andrew-lyster-huerco-s-31-july-2013', img: 'https://media2.ntslive.co.uk/resize/1600x1600/c24e0b2e-7f63-47ee-af15-088676091cc5_1461711600.jpeg'},
    {url: 'https://www.nts.live/shows/christopher-kirkley/episodes/autotune-the-world-w-christopher-kirkley-13th-april-2017/', show:  'autotune-the-world-w-christopher-kirkley-13th-april-2017', img: 'https://media2.ntslive.co.uk/resize/1600x1600/e053f8bf-de16-4627-8c4f-677117d81d2f_1489708800.jpeg'},
    {url: 'https://www.nts.live/shows/house-of-trax/episodes/trax-couture-w-rushmore-28th-june-2019/', show:  'trax-couture-w-rushmore-28th-june-2019', img: 'https://media2.ntslive.co.uk/resize/1600x1600/386d6603-e6ef-448a-89da-0d26d54f8ef1_1561939200.jpeg'},
    {url: 'https://www.nts.live/shows/fervent-moon/episodes/fervent-moon-8th-february-2018/', show:  'fervent-moon-8th-february-2018', img: 'https://media2.ntslive.co.uk/resize/1600x1600/847d038a-4925-44fd-985d-4eb6aee7bd56_1432207759.jpg'},
    {url: 'https://www.nts.live/shows/rhythmsection/episodes/rhythm-section-9th-may-2018/', show:  'rhythm-section-9th-may-2018', img: 'https://media2.ntslive.co.uk/resize/1600x1600/c4394dc9-2411-4907-870d-dcbd762bf823_1525910400.jpeg'},
    {url: 'https://www.nts.live/shows/benedek/episodes/benedek-5th-april-2019/', show:  'benedek-5th-april-2019', img: 'https://media2.ntslive.co.uk/resize/1600x1600/aa0a4f1c-0d6b-4ff9-8c9c-2949abdd38f9_1484611200.jpeg'},
    {url: 'https://www.nts.live/shows/fred-p/episodes/soul-people-music-radio-w-fred-p-31st-may-2017/', show:  'soul-people-music-radio-w-fred-p-31st-may-2017', img: 'https://media2.ntslive.co.uk/resize/1600x1600/7edccdd4-3422-4f18-a55f-0fc8f0f1d6fc_1481760000.jpeg'},
    {url: 'https://www.nts.live/shows/astral-plane/episodes/astral-plane-31st-august-2018/', show:  'astral-plane-31st-august-2018', img: 'https://media2.ntslive.co.uk/resize/1600x1600/a2145728-5a57-4564-a9a1-dc947d1d532b_1536192000.jpeg'},
    {url: 'https://www.nts.live/shows/the-extended-play-sessions/episodes/the-extended-play-sessions-9th-july-2016/', show:  'the-extended-play-sessions-9th-july-2016', img: 'https://media2.ntslive.co.uk/resize/1600x1600/99a7a608-afda-4030-b095-35205679af75_1563753600.jpeg'}
  ]

  constructor() { }

  ngOnInit(): void {
    console.log('Suggestions initialised...');
  }

}
