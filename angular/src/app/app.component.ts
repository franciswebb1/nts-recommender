import { Component, OnChanges } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  showsFound: boolean = false;
  badResponse: boolean = true;
  userInput: boolean = true;

  url: string;
  host: string;
  show: string;
  shows: string[];
  serverData: object;
  httpOptions: object = {
    headers: 'string',
    responseType: 'json'
  };
  suggestion: boolean = false;

  constructor(private httpClient: HttpClient){
  }

  public serverResponse(serverData) {
    console.log("Response recieved in parent..");
    console.log("Server response: ", serverData)
    if (serverData != 1) {
      this.userInput = false;
      this.serverData = serverData;
      this.showsFound = true;
      this.badResponse = false;
      this.suggestion = false;
    }
    else {
      this.userInput = false;
      this.badResponse = true;
      this.showsFound = false;
      this.suggestion = false;
    }
  }

  public displaySuggestions() {
    this.suggestion = true;
    this.userInput = false;
    this.badResponse = false;
    this.showsFound = false;
  }

  ngOnChanges() {
    console.log('hello');
  }

  reset(check){
    if (check == true) {
      this.userInput = true;
      this.badResponse = true;
      this.showsFound = false;
      this.suggestion = false;
    }
  }

}
