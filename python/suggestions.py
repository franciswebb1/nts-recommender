import os
import pandas as pd
from sklearn.cluster import KMeans
from sklearn.neighbors import NearestNeighbors


out_path = os.path.abspath(os.path.join(os.path.dirname(__file__)))


class Suggestions():
    def get(self):
        number_of_clusters = 16
        kmeans = KMeans(n_clusters=number_of_clusters, random_state=0).fit(show_average_data)
        show_averages['cluster'] = kmeans.labels_
        sample_shows = pd.DataFrame()
        for i in range(number_of_clusters):
            cluster = show_averages[show_averages['cluster'] == i]
            shows = cluster.sample(n=3)
            sample_shows = sample_shows.append(shows)
        sample_shows.to_csv('sample_shows.csv', index=False)
        print(sample_shows)
        #TODO make this a flask app that can be called from website to generate new suggestions
        #might need to make sure the random seed is a bit more random if so...
        return sample_shows


if __name__ == '__main__':
    drop_columns = ['date', 'host', 'Url']
    show_averages = pd.read_csv(f'{out_path}/show_features.csv')
    show_averages.dropna(inplace=True)
    show_average_data = show_averages.drop(drop_columns, axis=1)

    suggestions = Suggestions()
    suggestions.get()
