import requests
import boto3
import pandas as pd
import scipy.spatial as spatial

from bs4 import BeautifulSoup
from io import StringIO

from flask import Flask
from flask_cors import CORS
from flask_restful import Resource, Api
from flask_jsonpify import jsonify

from resources.secrets import AWS_ID, AWS_SECRET


aws_id = AWS_ID
aws_secret = AWS_SECRET

client = boto3.client('s3', aws_access_key_id=aws_id, aws_secret_access_key=aws_secret)
bucket_name = 'ntsshowsbucket'

object_key = 'show_features.csv'
csv_obj = client.get_object(Bucket=bucket_name, Key=object_key)

body = csv_obj['Body']
csv_string = body.read().decode('utf-8')

drop_columns = ['date', 'host', 'Url']
show_averages = pd.read_csv(StringIO(csv_string))
show_averages.dropna(inplace=True)
show_average_data = show_averages.drop(drop_columns, axis=1)

application = Flask(__name__)
api = Api(application)

CORS(application)


@application.route("/")
def hello():
    return jsonify({"Text": "Recommendation service..."})


class Recommender(Resource):

    def __init__(self):
        self.count = 0

    def prepare_url(self, host, show):
        show = "https://www.nts.live/shows/" + host + "/episodes/" + show
        if show[-1] != '/':
            show += '/'
        return show

    def backgroundImage(self, showUrl):
        try:
            r = requests.get(showUrl)
            html = BeautifulSoup(r.text)
            imageUrl = html.find("div", {"class": "mobile-show-image"})
            return imageUrl["style"].split("(", 1)[1].split(")", 1)[0]
        except:
            html = "Connection error - 111"
            return 1

    def get(self, host='', show=''):
        show = self.prepare_url(host=host, show=show)
        tree = spatial.KDTree(show_average_data)
        show_of_interest = show_averages[show_averages['Url'] == show]
        if show_of_interest.empty:
            return 1
        neighbours = nearest_neighbours(tree=tree, show_of_interest=show_of_interest, cluster=show_averages)['Url'].to_dict()
        for url in neighbours:
            neighbours[url] = [neighbours[url], neighbours[url] .split('/')[-2], self.backgroundImage(showUrl=neighbours[url])]
        return neighbours


class Suggestions(Resource):
    def backgroundImage(self, showUrl):
        try:
            r = requests.get(showUrl)
            html = BeautifulSoup(r.text)
            imageUrl = html.find("div", {"class": "mobile-show-image"})
            return imageUrl["style"].split("(", 1)[1].split(")", 1)[0]
        except:
            html = "Connection error - 111"
            return 1

    def get(self):
        number_of_clusters = 16
        kmeans = KMeans(n_clusters=number_of_clusters, random_state=0).fit(show_average_data)
        show_averages['cluster'] = kmeans.labels_
        sample_shows = pd.DataFrame()
        for i in range(number_of_clusters):
            cluster = show_averages[show_averages['cluster'] == i]
            shows = cluster.sample(n=3)
            sample_shows = sample_shows.append(shows)
        sample_shows = sample_shows['Url'].to_dict()
        for show in sample_shows:
            sample_shows[show] = [sample_shows[show], sample_shows[show] .split('/')[-2],
                                  self.backgroundImage(showUrl=sample_shows[show])]
        return sample_shows


def nearest_neighbours(tree, show_of_interest, cluster):
    nearest_neighbours = tree.query(show_of_interest.drop(columns=drop_columns), k=6)
    nn_indexes = nearest_neighbours[1][0]
    recommendations = pd.DataFrame()
    for i in range(len(nn_indexes)):
        if nearest_neighbours[0][0][i] != 0:
            nearby_url = cluster.iloc[nearest_neighbours[1][0][i]]['Url']
            recommendations = recommendations.append(show_averages[show_averages['Url'] == nearby_url])
    return recommendations


api.add_resource(Recommender, '/api/v1/recommender/<string:host>/<string:show>')
api.add_resource(Suggestions, '/api/v1/suggestions/')


if __name__ == '__main__':
    application.run()

